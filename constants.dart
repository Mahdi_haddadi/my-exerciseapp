import 'package:flutter/material.dart';

const kBackgroundColor=Color(0xFFeeeeee);
const kActiveIconColor=Color(0xFFff5722);
const kTextColor=Color(0xFF303841);
const kLightBlueColor=Color(0xFF00adb5);
const kShadowColor=Colors.black26;

BoxShadow buildBoxShadow() {
  return BoxShadow(
      color: kShadowColor,
      offset: Offset(0,17),
      spreadRadius: -10,
      blurRadius: 17
  );
}
